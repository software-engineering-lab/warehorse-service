const express = require("express");
const router = express.Router();

router.post("/", function(req, res) {
    const db = req.app.locals.db;
    const itemID  = req.body.itemID;
    db.collection("stock").update(
        { _id: { $eq: itemID } },
        { $set: { status: "Shipped" } }
    );
    return res.sendStatus(202);
})

module.exports = router;