const express = require("express");
const router = express.Router();

function createInventoryCollection(inventory) {
    const startSection =  inventory.startSection;
    const endSection = inventory.endSection;
    const building = inventory.building;
    const invtType = inventory.type;
    const maxLevel = inventory.levels;
    const maxBay = inventory.bays;
    const maxShelf = inventory.shelfs;
    sectionLength = endSection.charCodeAt(0) - startSection.charCodeAt(0);
    var sectionList = []
    for (var sec = 0; sec < (sectionLength + 1); sec++) {
        sectionList.push(String.fromCharCode(startSection.charCodeAt(0) + sec))
    }
    var inventoryCollection = {
        "_id": building,
        "type": invtType,
        "section": sectionList,
        "level": maxLevel,
        "bay": maxBay,
        "shelf": maxShelf,
    };
    for (var sec = 0; sec < (sectionLength + 1); sec++) {
        for (var lev = 1; lev <= maxLevel; lev++) {
            for (var bay = 1; bay <= maxBay; bay++) {
                for (var shelf = 1; shelf <= maxShelf; shelf++) {
                    letter = String.fromCharCode(startSection.charCodeAt(0) + sec);
                    key = letter + String(lev) + String(bay) + String(shelf);
                    inventoryCollection[key] = {
                        "_id": key,
                        "building": building,
                        "section": letter,
                        "level": lev,
                        "bay": bay,
                        "shelf": shelf,
                        "status": "available",
                        "stockId": "",
                    }
                }
            }
        }
    }
    return inventoryCollection;
}

function suggestLocation(inventory) {
    const sectionList = inventory["section"];
    const location = [];
    let i = 0;
    for (const invnt in inventory) {
        if (notEqualThis(invnt)) {
            continue;
        }
        if (inventory[invnt]["section"] === sectionList[i] && 
            inventory[invnt]["status"] === "available") {
            inventory[invnt]["no"] = i + 1;
            location.push(inventory[invnt]);
            i++;
        }
        if (i === sectionList.length) {
            break;
        }
    }
    return location;
}

function notEqualThis(key) {
    return (key === "_id" || 
            key === "type" ||
            key === "section" ||
            key === "level" ||
            key === "bay" ||
            key === "shelf");
}


router.post("/", function(req, res) {
    const newInventory = req.body;
    const db = req.app.locals.db;
    const inventoryCollection = createInventoryCollection(newInventory);
    db.collection("inventory").insert(inventoryCollection, function(err, res) {
        if (err) {
            throw(err)
        }
    });
    return res.sendStatus(200);
})

router.get("/all", async function(req, res) {
    const db = req.app.locals.db;
    const inventory = await db.collection("inventory").find().toArray();
    if (inventory) {
        return res.json(inventory);
    }
    else {
        return res.sendStatus(404);
    }
})

router.post("/find", async function(req, res) {
    const db = req.app.locals.db;
    const stock = req.body;
    const stockType = stock.itemType;
    const inventory = await db.collection("inventory").find({ type: stockType }).toArray();
    const location = suggestLocation(inventory[0]);
    var resData = {
        ...stock,
        suggestion: location,
    }
    return res.json(resData);
    
})

module.exports = router;