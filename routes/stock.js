const express = require("express");
const router = express.Router();

router.get("/", async function(req, res) {
    const db = req.app.locals.db;
    const stocks = await db.collection("stock").find().toArray();
    if (stocks) {
        return res.json(stocks);
    }
    else {
        return res.sendStatus(404);
    }
})

router.post("/", function(req, res) {
    const stockForm = req.body;
    const db = req.app.locals.db;
    delete stockForm.location.no;
    delete stockForm.location.status;
    delete stockForm.location.stockId;
    stockForm["checkInDate"] = new Date(stockForm["checkInDate"]);
    stockForm["checkOutDate"] = new Date(stockForm["checkOutDate"]);
    stockForm["status"] = "in-stock"
    db.collection("stock").insertOne({ ...stockForm }, function(err, res) {
        if (err) {
            res.status(400).json(err)
        }
    });
    const location = stockForm.location;
    const key = String(location.section) + String(location.level) + String(location.bay) + String(location.shelf);
    db.collection("inventory").updateOne( 
        { [`${key}._id`]: key }, 
        { $set: { [`${key}.status`]: "reserved", [`${key}.stockId`]: stockForm.itemID } }, 
        function(err, res) {
        if (err) {
            res.status(400).json(err)
        }
    });
    return res.sendStatus(202);
})

module.exports = router;