const cron = require("node-cron");

function updateStockStatus(db) {
    cron.schedule("*/10 * * * * *", async function() {
        const currentDateTime = new Date();
        await db.collection("stock").update(
            { $and: [ { checkOutDate: { $gte: currentDateTime } }, { status: { $eq: "in-stock" } } ] },
            { $set: { status: "Ready-to-Ship" } }
        )
    });
}

module.exports = updateStockStatus;