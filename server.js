const express = require("express");
const mongodb = require("mongodb");
const cors = require("cors");
const bodyParser = require("body-parser");

const config = require("./config");
const stock = require("./routes/stock");
const inventory = require("./routes/inventory");
const updateStockStatus = require("./routes/shipping-status");
const stockStatus = require("./utils/updateStockStatus");

const app = express();
const MongoClient = mongodb.MongoClient;

app.use(cors());
app.use(bodyParser.json());

app.use("/stock", stock);
app.use("/inventory", inventory);
app.use("/stock-status", updateStockStatus);

MongoClient.connect(config.database.url, function(err, mongo) {
        if (err) throw err;
        app.locals.db = mongo.db();
        stockStatus(app.locals.db);
        app.listen(config.port, function() {
            console.log("Running on http://0.0.0.0:8080");
        });
    }
);
